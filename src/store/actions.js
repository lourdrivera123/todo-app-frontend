export const getAllMessages = ({ commit }, messages) => {
  commit('GET_ALL_MESSAGES', messages)
}

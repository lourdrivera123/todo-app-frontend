import feathers from 'feathers/client'
import socketio from 'feathers-socketio/client'
import io from 'socket.io-client'
import store from './../store'

const socket = io('http://localhost:3030')

export const app = feathers().configure(socketio(socket))

export const getAllMessage = () => (
  new Promise((resolve, reject) => {
    app.service('messages')
    .find({})
    .then((response) => {
      store.dispatch('getAllMessages', response)
      resolve(response)
    })
    .catch((error) => {
      reject(error)
    })
  })
)

export const createMessage = (data) => (
  new Promise((resolve, reject) => {
    app.service('messages')
    .create(data)
    .then((response) => {
      resolve(response)
    })
    .catch((error) => {
      reject(error)
    })
  })
)

export const messageService = app.service('messages')
